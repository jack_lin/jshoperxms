package com.jshoperxms.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the goods_detail_rp_t database table.
 * 
 */
@Entity
@Table(name="goods_detail_rp_t")
@NamedQuery(name="GoodsDetailRpT.findAll", query="SELECT g FROM GoodsDetailRpT g")
public class GoodsDetailRpT implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private String id;

	@Lob
	private String detail;

	private String goodsid;

	public GoodsDetailRpT() {
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDetail() {
		return this.detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public String getGoodsid() {
		return this.goodsid;
	}

	public void setGoodsid(String goodsid) {
		this.goodsid = goodsid;
	}

}