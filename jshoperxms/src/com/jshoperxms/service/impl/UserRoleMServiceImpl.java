package com.jshoperxms.service.impl;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.jshoperxms.entity.UserRoleM;
import com.jshoperxms.service.UserRoleMService;
@Service("userRoleMService")
@Scope("prototype")
public class UserRoleMServiceImpl extends BaseTServiceImpl<UserRoleM> implements UserRoleMService {

}
