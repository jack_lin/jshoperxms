package com.jshoperxms.service.impl;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.jshoperxms.entity.FeedbackT;
import com.jshoperxms.service.FeedbackTService;

@Service("feedbackTService")
@Scope("prototype")
public class FeedbackTServiceImpl extends BaseTServiceImpl<FeedbackT> implements FeedbackTService {

}
