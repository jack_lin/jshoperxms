package com.jshoperxms.service.impl;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.jshoperxms.entity.MemberGroupT;
import com.jshoperxms.service.MemberGroupTService;

@Service("memberGroupTService")
@Scope("prototype")
public class MemberGroupTServiceImpl extends BaseTServiceImpl<MemberGroupT>implements MemberGroupTService{

}
