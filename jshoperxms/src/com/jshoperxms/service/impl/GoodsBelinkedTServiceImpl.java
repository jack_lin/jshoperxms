package com.jshoperxms.service.impl;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.jshoperxms.entity.GoodsBelinkedT;
import com.jshoperxms.service.GoodsBelinkedTService;

@Service("goodsBelinkedTService")
@Scope("prototype")
public class GoodsBelinkedTServiceImpl extends BaseTServiceImpl<GoodsBelinkedT>
		implements GoodsBelinkedTService {

}
