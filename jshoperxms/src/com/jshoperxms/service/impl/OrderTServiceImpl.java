package com.jshoperxms.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.jshoperxms.action.utils.statickey.StaticKey;
import com.jshoperxms.dao.CartTDao;
import com.jshoperxms.dao.OrderInvoiceTDao;
import com.jshoperxms.dao.OrderTDao;
import com.jshoperxms.dao.ShippingAddressTDao;
import com.jshoperxms.entity.CartT;
import com.jshoperxms.entity.OrderInvoiceT;
import com.jshoperxms.entity.OrderT;
import com.jshoperxms.entity.ShippingAddressT;
import com.jshoperxms.service.OrderTService;
@Service("orderTService")
@Scope("prototype")
public class OrderTServiceImpl extends BaseTServiceImpl<OrderT>implements OrderTService {
	@Resource
	private OrderTDao orderTDao;
	@Resource
	private Serial serial;
	@Resource
	private ShippingAddressTDao shippingAddressTDao;
	@Resource
	private CartTDao cartTDao;
	@Resource
	private OrderInvoiceTDao orderInvoiceTDao;

	
	@Override
	@Transactional(propagation=Propagation.REQUIRED)
	public void saveNormalOrderNeedInfoBack(OrderT ordert,
			ShippingAddressT sAddressT,List<CartT> cartLists,OrderInvoiceT oit) {
		//生成一个订单号
		String orderid=this.serial.Serialid(Serial.ORDER);
		//生成一个同批次购物车号
		String cartid=this.serial.Serialid(Serial.CART);
		//加入购物车表
		for(CartT c:cartLists){
			c.setOrderid(orderid);
			c.setCartid(cartid);
			c.setState(StaticKey.CARTSTATE_RELBYORDER_NUM);
			this.cartTDao.save(c);
		}
		//加入到发货地址表中
		sAddressT.setOrderid(orderid);
		this.shippingAddressTDao.save(sAddressT);
		//加入订单表
		ordert.setOrderid(orderid);
		this.orderTDao.save(ordert);
		//加入订单发票表
		oit.setOrderInvoiceid(this.serial.Serialid(Serial.ORDERINVOICE));
		oit.setOrderid(orderid);
		this.orderInvoiceTDao.save(oit);
	}



	
	
}
