package com.jshoperxms.service.impl;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.jshoperxms.entity.LogisticsBusinessT;
import com.jshoperxms.service.LogisticsBTService;

@Service("logisticsBTService")
@Scope("prototype")
public class LogisticsBTServiceImpl extends BaseTServiceImpl<LogisticsBusinessT>implements LogisticsBTService {

}
