package com.jshoperxms.service.impl;


import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.jshoperxms.entity.LogisticsbusinessareaT;
import com.jshoperxms.service.LogisticsBAreaTService;

@Service("logisticsBAreaTService")
@Scope("prototype")
public class LogisticsBAreaTServiceImpl extends BaseTServiceImpl<LogisticsbusinessareaT>implements LogisticsBAreaTService {

}
