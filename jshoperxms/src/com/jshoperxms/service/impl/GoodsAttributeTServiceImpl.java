package com.jshoperxms.service.impl;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.jshoperxms.entity.GoodsAttributeT;
import com.jshoperxms.service.GoodsAttributeTService;

@Service("goodsAttributeTService")
@Scope("prototype")
public class GoodsAttributeTServiceImpl extends BaseTServiceImpl<GoodsAttributeT>implements GoodsAttributeTService {

}
