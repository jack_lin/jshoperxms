package com.jshoperxms.dao.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.jshoperxms.dao.ArticleTDao;
import com.jshoperxms.entity.ArticleT;

@Repository("articleTDao")
public class ArticleTDaoImpl extends BaseTDaoImpl<ArticleT> implements ArticleTDao {

	private static final Logger log = LoggerFactory.getLogger(ArticleTDaoImpl.class);

	
}