package com.jshoperxms.dao.impl;

import org.springframework.stereotype.Repository;

import com.jshoperxms.dao.BasicUserTDao;
import com.jshoperxms.entity.BasicUserT;
@Repository("basicUserTDao")
public class BasicUserTDaoImpl extends BaseTDaoImpl<BasicUserT> implements BasicUserTDao{

}
