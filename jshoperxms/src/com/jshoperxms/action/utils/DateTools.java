package com.jshoperxms.action.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jshoperxms.action.utils.statickey.StaticKey;

/**
 * 时间格式化处理
* @ClassName: DateTools 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author jcchen
* @date 2015年10月31日 上午10:37:04 
*
 */
public class DateTools {
	private static final Logger log = LoggerFactory.getLogger(DateTools.class);


	
	/**
	 * 获取系统时间 long
	 * 
	 * @return
	 * @throws ParseException
	 */
	public static long getSystemTimeLong() {
		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat(StaticKey.DF_YYYY_MM_DD_HH_MM_SS,Locale.CHINA);
		String dateString = formatter.format(date);
		ParsePosition pos = new ParsePosition(0);
		Date currenttime = formatter.parse(dateString, pos);
		return currenttime.getTime();
	}	
	
	/**
	 * 获取系统时间
	 * 
	 * @return
	 * @throws ParseException
	 */
	public static Date getSystemTime() {
		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat(StaticKey.DF_YYYY_MM_DD_HH_MM_SS,Locale.CHINA);
		String dateString = formatter.format(date);
		ParsePosition pos = new ParsePosition(0);
		Date currenttime = formatter.parse(dateString, pos);
		return currenttime;
	}

	/**
	 * 转换数据库日期格式
	 * 
	 * @param object
	 * @return
	 */
	public static String formateDbDate(Date object) {
		SimpleDateFormat formatter = new SimpleDateFormat(StaticKey.DF_YYYY_MM_DD_HH_MM_SS,Locale.CHINA);
		String dateString = formatter.format(object);
		return dateString;
	}
	
	
	
	public static Date formatString2DateYYMMDD(String dateString) throws ParseException {
		DateFormat dateFormat;
		dateFormat = new SimpleDateFormat(StaticKey.DF_FF_MM_DD, Locale.CHINA);
		dateFormat.setLenient(false);
		Date timeDate = dateFormat.parse(dateString);
		Date dateTime = new Date(timeDate.getTime());
		return dateTime;
	}
	
	/**
	 * 格式化日期
	 * @param str
	 * @return
	 * @throws ParseException
	 */
	public static Date formatString2DateYYYMMDDHHMMSS(String str) throws ParseException{
		DateFormat dateFormat;
		dateFormat = new SimpleDateFormat(StaticKey.DF_YYYY_MM_DD_HH_MM_SS, Locale.CHINA);
		dateFormat.setLenient(false);
		String strs=StringUtils.replaceChars(str, "/", "-");
		Date timeDate = dateFormat.parse(strs);
		Date dateTime = new Date(timeDate.getTime());
		return dateTime;
	}
}
