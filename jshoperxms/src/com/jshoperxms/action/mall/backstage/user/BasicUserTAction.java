package com.jshoperxms.action.mall.backstage.user;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.InterceptorRefs;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;

import com.jshoperxms.action.mall.backstage.base.BaseTAction;
import com.jshoperxms.action.utils.SHA1;
import com.jshoperxms.action.utils.enums.BaseEnums;
import com.jshoperxms.action.utils.statickey.StaticKey;
import com.jshoperxms.action.utils.statickey.WarnWrods;
import com.jshoperxms.entity.BasicUserT;
import com.jshoperxms.redis.service.RedisBaseTService;
import com.jshoperxms.service.BasicUserTService;
import com.opensymphony.xwork2.ActionContext;

/**
 * 
* @ClassName: BasicUserT 
* @Description: 后台基础用户操作
* @author jcchen
* @date 2015年10月28日 下午4:41:58 
*
 */
@Namespace("/mall/user")
@ParentPackage("jshoperxms")
@InterceptorRefs({ @InterceptorRef("mallstack") })
public class BasicUserTAction extends BaseTAction {
	@Resource
	private BasicUserTService basicUserTService;
	@Resource
	private RedisBaseTService<BasicUserT> redisBaseTService;
	
	/**
	 * 后台管理者登录
	 * @return
	 */
	@Action(value = "login", results = { @Result(name = "json", type = "json") })
	public String login(){
		if (StringUtils.isBlank(this.getUsername())) {
			this.setMsg(WarnWrods.EMPTY_LOGINNAME);
			return JSON;
		}
		if (StringUtils.isBlank(this.getPassword())) {
			this.setMsg(WarnWrods.EMPTY_LOGINPWD);
			return JSON;
		}
		BasicUserT basicUserT=new BasicUserT();
		basicUserT.setUsername(StringUtils.trim(this.getUsername()));
		basicUserT.setPassword(SHA1.getDigestOfString(this.getPassword()));
		basicUserT.setUserstatus(BaseEnums.UserState.ACTIVE.getState());
		basicUserT.setStatus(BaseEnums.DataUsingState.USING.getState());
		Map<String,String>params=new HashMap<String,String>();
		params.put("username", StringUtils.trim(this.getUsername()));
		params.put("password", SHA1.getDigestOfString(this.getPassword()));
		params.put("userstatus",BaseEnums.UserState.ACTIVE.getState());
		params.put("status", BaseEnums.DataUsingState.USING.getState());
		Criterion criterion=Restrictions.allEq(params);
		basicUserT=this.basicUserTService.findOneByCriteria(BasicUserT.class, criterion);
		if(basicUserT!=null){
			doSysIndexInit(basicUserT);
			this.setSucflag(true);
		}else{
			this.setMsg(WarnWrods.EMPTY_ACCOUNT);
		}
		return JSON;
	}
	
	/**
	 * 进行用户登录有的系统首页数据初始化及用户权限相关初始化
	 */
	private void doSysIndexInit(BasicUserT basicUserT) {
		ActionContext.getContext().getSession()
				.put(StaticKey.BACK_USER_SESSION_KEY, basicUserT);
		this.setToken(basicUserT.getId());
		redisBaseTService.put(basicUserT.getId(), basicUserT, BasicUserT.class, 30, TimeUnit.MINUTES);
//		Map<String,String>m=new HashMap<>();
//		m.put("1", "1");
//		m.put("2", "2");
//		redisBaseTService.put("hash", m, BasicUserT.class);
		
		this.setBean(basicUserT);
	}
	
	
	
	
	private String username;
	private String password;
	private String msg;
	private String token;
	
	private BasicUserT bean;
	
	private boolean sucflag;
	
	public BasicUserT getBean() {
		return bean;
	}

	public void setBean(BasicUserT bean) {
		this.bean = bean;
	}

	public boolean isSucflag() {
		return sucflag;
	}

	public void setSucflag(boolean sucflag) {
		this.sucflag = sucflag;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}



	
	
	
}
