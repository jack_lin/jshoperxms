用户反馈－－用户反馈&参数管理
===
##### 作者：SETH.Wang 日期：2015-10-13 版本：1.0

#### 功能简介
- 用户反馈模块，用来定义用户反馈包含的参数。

#### 数据模型
  源码路径:
  /jshoperxms/src/com/jshoperxms/entity/FeedbackT.java
  @Id
	private String id;

	private String content;

	private String status;

	private String devicetype;

	private String basicinfoid;

	@Temporal(TemporalType.TIMESTAMP)
	private Date createtime;

	@Temporal(TemporalType.TIMESTAMP)
	private Date updatetime;

	private int versiont;

	private String dataid;

	private String feedbacksource;

	private String feedbackpurpose;

#### 视图
    源码路径:
    /jshoperxms/WebContent/admin/app/feedback/feedbackment.html
    /jshoperxms/WebContent/admin/app/feedback/feedback.html

#### 控制层
    源码路径:
    /com/jshoperxms/action/mall/backstage/feedback/FeedbackTAction.java
    /jshoperxms/WebContent/admin/app/js/feedback/feedbackctrl.js

    //列表查询
    feedbackmodule.controller('feddbacklist',ServerFeedbackListCtrl);

    //添加和编辑
    feedbackmodule.controller('feedback',ServerSaveFeedback);

    //所有业务控制均采用angularJs调用后台java方法，并统一返回JSON对象

#### 路由控制
    源码路径:
    /jshoperxms/WebContent/admin/app/js/routes.js
    /**
      * 用户反馈管理
      */
    $routeProvider.when('/feedbackment',{
    	templateUrl:'app/feedback/feedbackment.html',
    	controller:'feedbacklist'
    });
    $routeProvider.when('/feedback',{
    	templateUrl:'app/feedback/feedback.html',
    	controller:'feedback'
    });

#### 插件使用
- angularjs-datatables.js
