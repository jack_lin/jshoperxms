define(['./module'],function(indexmodule){
	'use strict';
	indexmodule.directive("topnav", function() {
		return {
			restrict : 'E',
			templateUrl : 'app/tpls/top_nav_tpl.html',
			replace : true
		}
	});
	indexmodule.directive("leftnav", function() {
		return {
			restrict : 'E',
			templateUrl : 'app/tpls/left_nav_tpl.html'
		}
	});
	indexmodule.directive("footernav", function() {
		return {
			restrict : 'E',
			templateUrl : 'app/tpls/footer_tpl.html'
		}
	});
	/*=====Begin Of Page Element Directive =====*/
	indexmodule.directive('alertdanger',function(){
		return{
			restrict:'E',
			templateUrl:'../admin/app/tpls/alert_danger_tpl.html',
			replace:true
		}
	});
	indexmodule.directive('alertsuccess',function(){
		return{
			restrict:'E',
			templateUrl:'../admin/app/tpls/alert_success_tpl.html',
			replace:true
		}
	});
	/*=====End Of Page Element Directive=====*/
	indexmodule.controller('index', [ '$scope', '$http','$window','$location', function($scope, $http,$window,$location) {
		$scope.title='首页';
	} ]);

	indexmodule.controller('top', ['$rootScope','$scope', '$http','$window','$location', function($rootScope,$scope, $http,$window,$location) {
		/**
		 * 退出
		 */
		$scope.loginout=function(){
			$window.localStorage.clear();
			window.location.href='login.html';
		}
	} ]);

	indexmodule.controller('left', ['$scope', '$http','$window','$location', function($scope, $http,$window,$location) {
		if($window.localStorage){
			
			var basicuser=localStorage.getItem('basicuser');
			if(basicuser!=null&&basicuser!=undefined){
				$scope.username=angular.fromJson(basicuser).username;
			}
		}
		
	} ]);
});
