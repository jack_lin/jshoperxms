define([ './module' ], function(brandmodule) {
	'use strict';


	
	brandmodule.controller('fileupload',['$scope','Upload','$timeout',function($scope,Upload,$timeout){
		$scope.$watch('files',function(){
			$scope.upload($scope.files);
		});
		$scope.log='';
		$scope.upload=function(files){
			if(files&&files.length){
				for(var i=0;i<files.length;i++){
					var file=files[i];
					if(!file.$error){
						Upload.upload({
							url:'../mall/files/ajaxFileUploads.action',
							data:{
								file:file
							}
						}).progress(function (evt) {
		                    var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
		                    $scope.log = 'progress: ' + progressPercentage + '% ' +
		                                evt.config.data.file.name + '\n' + $scope.log;
		                }).success(function (data, status, headers, config) {
		                    $timeout(function() {
		                        $scope.log = 'file: ' + config.data.file.name + ', Response: ' + JSON.stringify(data) + '\n' + $scope.log;
		                    });
		                });
					}
				}
			}
		}
	}]);
	
	
	
	
	
	
	
	
	
	/*=====Begin Of Save and Edit GoodsTypeT=====*/
	brandmodule.controller('brand',ServerSaveBrand);
	function ServerSaveBrand($scope,$http,$location){

		//通过location中的operate参数区分操作行为
		var operate=$location.search().operate;
		if(operate!=undefined&&operate=="save"){
			//控制保存按钮显示
			$scope.savebtn={show:true};
			$scope.title='添加品牌';
			
			$scope.status="1";



			//保存数据方法
			$scope.save=function(){
				if(validate()){
					var goodsTypeId=$scope.selectedgoodstype.goodsTypeId;
					var goodsTypeName=$scope.selectedgoodstype.name;
					var goodsattributename=$scope.goodsattributename;
					var issearch=$scope.issearch;
					var issametolink=$scope.issametolink;
					var weight=$scope.weight;
					var sort=$scope.sort;
					var attributelist=angular.toJson($scope.attrs);
					var status=$scope.status;
					$http({
						method:'POST',
						url:'../mall/goods/goodsattribute/save.action',
						data:{
							'goodsTypeId':goodsTypeId,
							'goodsTypeName':goodsTypeName,
							'goodsattributename':goodsattributename,
							'issearch':issearch,
							'issametolink':issametolink,
							'weight':weight,
							'sort':sort,
							'attributelist':attributelist,
							'status':status
						}
					}).
					success(function(data,status,headers,config){
						if(data.sucflag){
							$scope.info={
									show:true,
									msg:'商品属性添加成功'
							}
						}
					}).
					error(function(data,status,headers,config){
						$scope.errors={
								show:true,
								msg:'系统异常'
						}
					});
				}
			},
			$scope.remove=function(index){
				$scope.attrs.splice(index,1);
			}
		}
		//如果operate是edit则执行保存行为
		if(operate=='edit'){
			console.log("111");
			var id=$location.search().id;
			if(id!=undefined&&id!=""){
				$http({
					method:'POST',
					url:'../mall/goods/goodsattribute/find.action',
					data:{
						'goodsattributeid':id
					}
				}).
				success(function(data,status,headers,config){
					if(data.sucflag){
						$scope.title='正在对商品类型--'+data.bean.goodsTypeName+'--的属性--'+data.bean.goodsattributename+'--进行编辑';
						$scope.oldgoodstype=data.bean;
						$scope.goodsattributename=data.bean.goodsattributename;
						$scope.attrs=angular.fromJson(data.bean.attributelist);
						$scope.sort=data.bean.sort;
						$scope.issearch=data.bean.issearch;
						$scope.issametolink=data.bean.issametolink;
						$scope.weight=data.bean.weight;
						$scope.status=data.bean.status;
						$scope.updatebtn={show:true};
					}
				}).
				error(function(data,status,headers,config){
					$scope.errors={
							show:true,
							msg:'系统异常'
					}
				});
				//更新数据方法
				$scope.update=function(){
					var goodsTypeId;
					var goodsTypeName;
					if($scope.selectedgoodstype==undefined){
						goodsTypeId=$scope.oldgoodstype.goodsTypeId;
						goodsTypeName=$scope.oldgoodstype.goodsTypeName;
					}else{
						goodsTypeId=$scope.selectedgoodstype.goodsTypeId;
						goodsTypeName=$scope.selectedgoodstype.name;
					}
					var goodsattributename=$scope.goodsattributename;
					var issearch=$scope.issearch;
					var issametolink=$scope.issametolink;
					var weight=$scope.weight;
					var sort=$scope.sort;
					var attributelist=angular.toJson($scope.attrs);
					var status=$scope.status;
					$http({
						method:'POST',
						url:'../mall/goods/goodsattribute/update.action',
						data:{
							'goodsTypeId':goodsTypeId,
							'goodsTypeName':goodsTypeName,
							'goodsattributename':goodsattributename,
							'issearch':issearch,
							'issametolink':issametolink,
							'weight':weight,
							'sort':sort,
							'attributelist':attributelist,
							'status':status,
							'goodsattributeid':id
						}
					}).
					success(function(data,status,headers,config){
						if(data.sucflag){
							$scope.info={
									show:true,
									msg:'商品属性更新成功'
							}
						}
					}).
					error(function(data,status,headers,config){
						$scope.errors={
								show:true,
								msg:'系统异常'
						}
					});

				}

			}


		}
		//表单字段验证
		function validate(){
			var select=$scope.selectedgoodstype;
			if(select==undefined){
				$scope.errors={
						show:true,
						msg:'请选择商品类型'
				}
				return false;
			}
			var goodsattributename=$scope.goodsattributename;
			if(goodsattributename==undefined||goodsattributename==""){
				$scope.errors={
						show:true,
						msg:'请输入属性名称'
				}
				return false;
			}
			return true;
		}
	}
	/*=====End Of Save and Edit Brand=====*/

	/*=====Begin Of Find List Brand=====*/
	brandmodule.controller('brandlist',ServerGoodsTypeListCtrl);
	//查询列表数据
	function ServerGoodsTypeListCtrl($http,$location,$compile,$scope,$rootScope,$resource,DTOptionsBuilder,DTColumnBuilder,DTAjaxRenderer){
		var vm=this;
		vm.message='';
		vm.someClickHandler = someClickHandler;
		vm.selected={};
		vm.selectAll=false;
		vm.toggleAll=toggleAll;
		vm.toggleOne=toggleOne;
		vm.dtInstance={};
		var titleHtml='<input type="checkbox" ng-model="showCase.selectAll" ng-click="showCase.toggleAll(showCase.selectAll,showCase.selected)">';
		vm.dtOptions=DTOptionsBuilder.newOptions().withOption('ajax',{
			type:'POST',
			url:'../mall/goods/goodsattribute/findByPage.action',
			dataSrc:'data'
		})
		.withOption('processing',true)
		.withOption('paging',true)
		.withOption('serverSide',true)
		.withOption('createdRow',function(row,data,dataIndex){
			$compile(angular.element(row).contents())($scope);
		})
		.withOption('headerCallback', function(header) {
            if (!$scope.headerCompiled) {
                $scope.headerCompiled = true;
                $compile(angular.element(header).contents())($scope);
            }
        })
        .withOption('stateSave', true)
        .withOption('rowCallback',rowCallback)
		.withPaginationType('full_numbers')
		.withLanguageSource('./app/language/chinese.json')

		$scope.$on('handleRequest',function(){
			
		});
		$scope.getTableData=function serverData(){
			
		}
		vm.dtColumns=[
		              DTColumnBuilder.newColumn(null).withTitle(titleHtml).notSortable().renderWith(function(data,type,full,meta){
		            	  vm.selected[full.id]=false;
		            	  return '<input type="checkbox" ng-model="showCase.selected['+data.id+']" ng-click="showCase.toggleOne(showCase.selected)">';
		              }),
		              DTColumnBuilder.newColumn('id').withTitle('ID').notVisible(),
			          DTColumnBuilder.newColumn('goodsattributename').withTitle('名称').notSortable(),
			          DTColumnBuilder.newColumn('goodsTypeName').withTitle('所属类型').notSortable(),
			          DTColumnBuilder.newColumn('attributelist').withTitle('属性列表').notSortable(),
			          DTColumnBuilder.newColumn('attributeType').withTitle('属性类型').notSortable(),
			          DTColumnBuilder.newColumn('issearch').withTitle('搜索支持').notSortable(),
			          DTColumnBuilder.newColumn('issametolink').withTitle('属性检索支持').notSortable(),
			          DTColumnBuilder.newColumn('weight').withTitle('权重').notSortable(),
			          DTColumnBuilder.newColumn('sort').withTitle('排序').notSortable(),
			          DTColumnBuilder.newColumn('status').withTitle('状态').notSortable(),
			          DTColumnBuilder.newColumn('updatetime').withTitle('更新时间').notSortable(),
			          DTColumnBuilder.newColumn('version').withTitle('版本号').notSortable(),
			          DTColumnBuilder.newColumn(null).withTitle('操作').notSortable().renderWith(actionHtml)];
		function actionHtml(data,type,full,meta){
			return '<button class="btn btn-warning" ng-click="edit('+data.id+')"><i class="fa fa-edit"></i></button>';
		}
		//表格中编辑按钮
		$scope.edit=function(id,name){
			$location.path('/brand').search({'operate':'edit','id':id});
		}

		/**
		 * 跳转到添加商品类型和参数页面
		 */
		$scope.save=function(){
			$location.path('/brand').search({'operate':'save'});
		}

		$scope.del=function(){
			var i=0;
			var ids=[];
			angular.forEach(vm.selected, function(data,index,array){
				if(data){
					i++;
					ids.push(index);
				}
			});
			if(i==0){
				$scope.errors={
						show:true,
						msg:'请选择一条记录'
				}
			}else{
				$scope.errors={
						show:false
				}
				//批量删除数据
				var idstrs=ids.join(",");
				$http({
					method:'POST',
					url:'../mall/goods/goodsattribute/del.action',
					data:{
						'ids':idstrs
					}
				}).
				success(function(data,status,headers,config){
					if(data.sucflag){
						$scope.info={
								show:true,
								msg:'删除成功'
						}
						$location.path('/goodsattributement');
					}

				}).
				error(function(data,status,headers,config){
					$scope.errors={
							show:true,
							msg:'系统异常'
					}
				});
			}
		}

		/**
		 * 列表全选
		 */
		function toggleAll(selectAll,selectedItems){
			for(var id in selectedItems){
				if(selectedItems.hasOwnProperty(id)){
					selectedItems[id]=selectAll;
				}
			}
		}
		/**
		 * 列表单选
		 */
		function toggleOne(selectedItems){
			var me=this;
			for(var id in selectedItems){
				if(selectedItems.hasOwnProperty(id)){
					if(!selectedItems[id]){
						me.selectAll=false;
					}
				}
			}
			me.selectAll=true;
		}

		function someClickHandler(info) {
	        vm.message = info.id + ' - ' + info.name;
	    }
		/**
		 * 单击列表某行回调
		 */
		function rowCallback(nRow,aData,iDisplayIndex,iDisplayIndexFull){
			$('td', nRow).unbind('click');
	        $('td', nRow).bind('click', function() {
	            $scope.$apply(function() {
	                vm.someClickHandler(aData);
	            });
	        });
	        return nRow;
		}

	}
});
