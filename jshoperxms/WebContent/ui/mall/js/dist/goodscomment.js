$(function(){
  $("#emsg").text("");
  savegoodscomment=function(basepath,gid){
    var score=$("li[class='on']").length;
    if(score==0){
      $("#emsg").text("请选择星级");
      return false;
    }
    var goodsid=gid;
    var commentcontent=$("#commentcontent").val();
    if(commentcontent==""){
      $("#emsg").text("请输入评价内容");
      return false;
    }
    if(commentcontent.length>200){
      $("#emsg").text("评价内容太长，请小于200个字符");
      return false;
    }
    $.post(basepath+"/goods/saveGoodsComment.action",{
      "goodsid":goodsid,
      "score":score,
      "commentcontent":commentcontent
    },function(data){
      if(data.sucflag){
        $("#emsg").text("评论成功");
      }else{
        $("#emsg").text("请先登录然后评论");
      }
    });
  },
  buynow=function(basepath,goodsid){
    window.location.href=basepath+"/order/initOrder.action?goodsstr="+goodsid;
  }

});